package ru.akvine.foodelo_core;

public class RestMethods {
    public final static String API_PRODUCTS_LIST = "/api/v1/products";
    public final static String API_PRODUCTS_ADD = "/api/v1/products";

    public final static String API_ORDER_PERFORM = "/api/v1/orders/perform";
    public final static String API_ORDER_CANCEL = "/api/v1/orders/cancel/";
    public final static String API_ORDER= "/api/v1/orders/";
}
