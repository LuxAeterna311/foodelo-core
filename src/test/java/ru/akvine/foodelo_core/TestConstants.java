package ru.akvine.foodelo_core;

import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.util.Date;

public class TestConstants {
    public static String PRODUCT_NAME_1 = "name1";
    public static String PRODUCT_PRODUCER_1 = "producer1";
    public static String PRODUCT_CATEGORY_1 = "category1";
    public static String PRODUCT_UUID_1 = "6ee530266e1b4862845b8f581d89000001";
    public static String TEST_UUID_1 = "uuid1";
    public static long PRODUCT_PRICE_1 = 10000;
    public static int PRODUCT_COUNT_1 = 50;
    public static int PRODUCT_PAGE = 0;


    public static long INVALID_PRODUCT_PRICE = -10000;
    public static int INVALID_PRODUCT_COUNT = -50;
    public static int INVALID_PAGE = -1;
    public static int INVALID_LIMITS = -5;

    public static Date PRODUCTION_DATE_1;
    public static Date EXPIRATION_DATE_1;

    public static String ORDER_UUID_1 = "order_uuid_1";
    public static String ORDER_UUID_2 = "order_uuid_2";
    public static String ORDER_UUID_3 = "order_uuid_3";
    public static String ORDER_UUID_4 = "order_uuid_4";
    public static String ORDER_NOT_EXISTS = "invalid_uuid";
    public static long ORDER_AMOUNT_2 = 3000;
    public static long ORDER_AMOUNT_3 = 30000;
    public static String ORDER_STATUS_CREATED = "CREATED";
    public static String ORDER_STATUS_DECLINED = "DECLINED";
    public static String ORDER_STATUS_PAID = "PAID";

    static {
        try {
            PRODUCTION_DATE_1 = DateUtils.parseDate("2022-25-05");
            PRODUCTION_DATE_1 = DateUtils.parseDate("2022-08-05");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
