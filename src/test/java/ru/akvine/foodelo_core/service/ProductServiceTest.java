package ru.akvine.foodelo_core.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.akvine.foodelo_core.UnitBaseTest;
import ru.akvine.foodelo_core.domain.entity.ProductEntity;
import ru.akvine.foodelo_core.domain.entity.ProductItemEntity;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest extends UnitBaseTest {
    private ProductService productService;

    @BeforeEach
    void init() {
        productService = new ProductService(productRepository);
    }

    @Test
    void reduceCount() {
        int size = 5;
        List<ProductItemEntity> productItemEntities = generateProductItemEntities(size);
        List<ProductEntity> productEntities = new ArrayList<>();

        for (int i = 0; i < size; ++i) {
            productEntities.add(productItemEntities.get(i).getProduct());
        }
        mockProductSaveAll(productEntities);

        productService.reduceCount(productItemEntities);
        verify(productRepository, times(1)).saveAll(productEntities);
    }
}