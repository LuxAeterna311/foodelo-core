package ru.akvine.foodelo_core.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import ru.akvine.foodelo_core.UnitBaseTest;
import ru.akvine.foodelo_core.domain.entity.OrderEntity;
import ru.akvine.foodelo_core.rest.dto.converter.PerformOrder;
import ru.akvine.foodelo_core.service.dto.Order;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.Map;

class OrderServiceTest extends UnitBaseTest {
    private OrderService orderService;

    @BeforeEach
    void init() {
        orderService = new OrderService(
                orderRepository,
                calculateService,
                productService);
    }

    @Test
    void performOrder() {
        Map<String, Integer> products = generateProductsMap(3);
        PerformOrder performOrder = new PerformOrder()
                .setProducts(products);
        OrderEntity mockedOrderEntity = generateOrderEntities().get(0);

        mockCalculateAmount();
        mockOrderSave(mockedOrderEntity);

        Order actualOrder = orderService.performOrder(performOrder);
        Order expectedOrder = new Order(mockedOrderEntity);

        assertNotNull(actualOrder.getUuid());
        assertNotNull(actualOrder.getStatus());
        assertNotNull(actualOrder.getProducts());

        assertEquals(expectedOrder, actualOrder);
        assertEquals(actualOrder.getAmount(), expectedOrder.getAmount());
        assertEquals(actualOrder.getStatus(), expectedOrder.getStatus());
    }
}