package ru.akvine.foodelo_core;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.akvine.foodelo_core.repository.OrderRepository;
import ru.akvine.foodelo_core.repository.ProductRepository;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource(
        properties = {
                "spring.jpa.hibernate.ddl-auto=create-drop"
        }
)
public abstract class DataBaseTest {
    @Autowired
    protected OrderRepository orderRepository;
    @Autowired
    protected ProductRepository productRepository;
}
