package ru.akvine.foodelo_core;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.akvine.foodelo_core.domain.entity.OrderEntity;
import ru.akvine.foodelo_core.domain.entity.ProductEntity;
import ru.akvine.foodelo_core.domain.entity.ProductItemEntity;
import ru.akvine.foodelo_core.domain.models.OrderStatus;
import ru.akvine.foodelo_core.repository.OrderRepository;
import ru.akvine.foodelo_core.repository.ProductRepository;
import ru.akvine.foodelo_core.service.CalculateService;
import ru.akvine.foodelo_core.service.ProductService;
import ru.akvine.foodelo_core.service.dto.CalculateAmount;

import java.util.*;

@ExtendWith(MockitoExtension.class)
public abstract class UnitBaseTest {
    @Mock
    protected OrderRepository orderRepository;
    @Mock
    protected ProductService productService;
    @Mock
    protected CalculateService calculateService;
    @Mock
    protected ProductRepository productRepository;

    private static final int MIN_COUNTS = 1;
    private static final int MAX_COUNTS = 15;

    protected List<ProductEntity> generateProductEntities() {
        return generateProductEntities(1);
    }

    protected List<ProductEntity> generateProductEntities(int size) {
        return generateProductEntities(size, 10);
    }

    protected List<ProductEntity> generateProductEntities(int size, int count) {
        return generateProductEntities(size, count, 10000);
    }

    protected List<ProductEntity> generateProductEntities(int size, int count, long price) {
        return generateProductEntities(size, count, price, "");
    }

    protected List<ProductEntity> generateProductEntities(int size,
                                                          int count,
                                                          long price,
                                                          String category) {
        return generateProductEntities(size, count, price, category, generateUuidList(size));
    }

    protected List<ProductEntity> generateProductEntities(int size,
                                                          int count,
                                                          long price,
                                                          String category,
                                                          Collection<String> uuidCollection) {
        List<ProductEntity> productEntities = new ArrayList<>();
        for (int i = 0; i < size; ++i) {
            ProductEntity productEntity = new ProductEntity()
                    .setUuid(uuidCollection.iterator().next())
                    .setCategory(category)
                    .setCount(count)
                    .setExpirationDate(new Date())
                    .setProductionDate(new Date())
                    .setProducer("")
                    .setName("")
                    .setPrice(price)
                    .setDeleted(false)
                    .setDescription("");

            productEntities.add(productEntity);
        }

        return productEntities;
    }

    protected Map<String, Integer> generateProductsMap() {
        return generateProductsMap(1);
    }

    protected Map<String, Integer> generateProductsMap(int size) {
        return generateProductsMap(size, generateCounts(size));
    }

    protected Map<String, Integer> generateProductsMap(int size, Collection<Integer> counts) {
        List<String> uuidList = generateUuidList(size);
        Map<String, Integer> productsMap = new HashMap<>();

        for (String uuid : uuidList) {
            productsMap.put(uuid, counts.iterator().next());
        }

        return productsMap;
    }

    protected List<CalculateAmount> generateCalculateAmount() {
        return generateCalculateAmount(1);
    }

    protected List<CalculateAmount> generateCalculateAmount(int amountsSize) {
        return generateCalculateAmount(amountsSize, 10);
    }

    protected List<CalculateAmount> generateCalculateAmount(int amountsSize,
                                                            int productsSize) {
        return generateCalculateAmount(amountsSize, productsSize, 10000);
    }

    protected List<CalculateAmount> generateCalculateAmount(int amountsSize,
                                                            int productsSize,
                                                            long amount) {
        return generateCalculateAmount(amountsSize, productsSize, amount, generateProductEntities(productsSize));
    }

    protected List<CalculateAmount> generateCalculateAmount(int amountsSize,
                                                            int productsSize,
                                                            long amount,
                                                            List<ProductEntity> productEntities) {
        List<CalculateAmount> calculateAmounts = new ArrayList<>();

        for (int i = 0; i < amountsSize; ++i) {
            CalculateAmount calculateAmount = new CalculateAmount();
            calculateAmount.setAmount(amount);
            calculateAmount.setProducts(productEntities);
            calculateAmounts.add(calculateAmount);
        }

        return calculateAmounts;
    }

    protected List<OrderEntity> generateOrderEntities() {
        return generateOrderEntities(1);
    }

    protected List<OrderEntity> generateOrderEntities(int size) {
            return generateOrderEntities(size, 10000);
    }

    protected List<OrderEntity> generateOrderEntities(int size, long amount) {
        return generateOrderEntities(size, amount, OrderStatus.CREATED, generateUuidList(size));
    }

    protected List<OrderEntity> generateOrderEntities(int size, long amount, OrderStatus status) {
        return generateOrderEntities(size, amount, status, generateUuidList(size));
    }

    protected List<OrderEntity> generateOrderEntities(int size,
                                                      long amount,
                                                      OrderStatus status,
                                                      Collection<String> uuidCollection) {
        return generateOrderEntities(size, amount, uuidCollection, status, generateProductItemEntities(size));
    }

    protected List<OrderEntity> generateOrderEntities(int size,
                                                      long amount,
                                                      Collection<String> uuidCollection,
                                                      OrderStatus status,
                                                      List<ProductItemEntity> productItemEntities) {
        List<OrderEntity> orderEntities = new ArrayList<>();

        for (int i = 0; i < size; ++i) {
            OrderEntity orderEntity = new OrderEntity()
                    .setAmount(amount)
                    .setUuid(uuidCollection.iterator().next())
                    .setProducts(productItemEntities)
                    .setStatus(status);

            orderEntities.add(orderEntity);
        }

        return orderEntities;
    }

    protected List<ProductItemEntity> generateProductItemEntities() {
        return generateProductItemEntities(1);
    }

    protected List<ProductItemEntity> generateProductItemEntities(int size) {
        return generateProductItemEntities(size, 10, generateProductEntities(size));
    }

    protected List<ProductItemEntity> generateProductItemEntities(int size, int count) {
        return generateProductItemEntities(size, count, generateProductEntities(size));
    }


    protected List<ProductItemEntity> generateProductItemEntities(int size, int count, List<ProductEntity> productEntities) {
       List<ProductItemEntity> productItemEntities = new ArrayList<>();

       for (int i = 0; i < size; ++i) {
           ProductItemEntity productItemEntity = new ProductItemEntity();
           productItemEntity.setCount(count);
           productItemEntity.setProduct(productEntities.get(i));
           productItemEntities.add(productItemEntity);
       }

       return productItemEntities;
     }

    protected void mockCalculateAmount() {
        Mockito.when(calculateService.calculateAmount(ArgumentMatchers.any()))
                .thenReturn(generateCalculateAmount().get(0));
    }

    protected void mockCalculateAmount(CalculateAmount mockedCalculatedAmount) {
        Mockito.when(calculateService.calculateAmount(ArgumentMatchers.any()))
                .thenReturn(mockedCalculatedAmount);
    }

    protected void mockOrderSave() {
        Mockito.when(orderRepository.save(ArgumentMatchers.any()))
                .thenReturn(generateOrderEntities());
    }

    protected void mockOrderSave(OrderEntity mockedOrderEntity) {
        Mockito.when(orderRepository.save(ArgumentMatchers.any()))
                .thenReturn(mockedOrderEntity);
    }

    protected void mockProductSaveAll(List<ProductEntity> products) {
        Mockito.when(productRepository.saveAll(ArgumentMatchers.any()))
                .thenReturn(products);
    }

    private List<String> generateUuidList(int size) {
        String baseUuid = "uuid_";
        List<String> generatedList = new ArrayList<>();

        for (int i = 1; i < size + 1; ++i) {
            generatedList.add(baseUuid + i);
        }

        return generatedList;
    }

    private List<Integer> generateCounts(int size) {
        List<Integer> counts = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < size; ++i) {
            Integer randomCount = random.nextInt(MAX_COUNTS - MIN_COUNTS) + MIN_COUNTS;
            counts.add(randomCount);
        }

        return counts;
    }
}
