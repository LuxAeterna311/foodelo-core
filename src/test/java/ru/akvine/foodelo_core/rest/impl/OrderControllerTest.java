package ru.akvine.foodelo_core.rest.impl;

import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;
import ru.akvine.foodelo_core.ApiBaseTest;
import ru.akvine.foodelo_core.rest.dto.request.order.PerformOrderRequest;
import ru.akvine.foodelo_core.rest.dto.request.product.ProductRequestDto;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.akvine.foodelo_core.TestConstants.*;
import static ru.akvine.foodelo_core.RestMethods.*;

@Transactional
public class OrderControllerTest extends ApiBaseTest {

    @Test
    void perform_order_success() throws Exception {
        PerformOrderRequest request = new PerformOrderRequest()
                .setProducts(generateProductRequests());

        doPost(API_ORDER_PERFORM, request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"))
                .andExpect(jsonPath("$.order.uuid").isNotEmpty())
                .andExpect(jsonPath("$.order.status").value(ORDER_STATUS_CREATED));
    }

    @Test
    void perform_order_count_less_than_zero_fail() throws Exception {
        PerformOrderRequest request = new PerformOrderRequest()
                .setProducts(generateProductRequests(0));
        doPost(API_ORDER_PERFORM, request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value("validation.error"))
                .andExpect(jsonPath("$.message").value("Product's count can't be less than 0"));
    }

    @Test
    void perform_order_count_more_from_db_fail() throws Exception {
        PerformOrderRequest request = new PerformOrderRequest()
                .setProducts(generateProductRequests((int) 10e7));
        doPost(API_ORDER_PERFORM, request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value("reduce.count.error"))
                .andExpect(jsonPath("$.message").value("Reduce count can't be more than product count"));
    }

    @Test
    void cancel_order_success() throws Exception {
        String url = API_ORDER_CANCEL + ORDER_UUID_2;
        doGet(url, null)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"))
                .andExpect(jsonPath("$.order.uuid").value(ORDER_UUID_2))
                .andExpect(jsonPath("$.order.status").value(ORDER_STATUS_DECLINED))
                .andExpect(jsonPath("$.order.amount").value(ORDER_AMOUNT_2));
    }

    @Test
    void cancel_order_not_found_fail() throws Exception {
        String url = API_ORDER_CANCEL + ORDER_NOT_EXISTS;
        doGet(url, null)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value("order.notFound.error"))
                .andExpect(jsonPath("$.message").value("Order with uuid=" + ORDER_NOT_EXISTS + " not found!"));

    }

    @Test
    void find_order_by_uuid_success() throws Exception {
        String url = API_ORDER + ORDER_UUID_3;
        doGet(url, null)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"))
                .andExpect(jsonPath("$.order.uuid").value(ORDER_UUID_3))
                .andExpect(jsonPath("$.order.status").value(ORDER_STATUS_PAID))
                .andExpect(jsonPath("$.order.amount").value(ORDER_AMOUNT_3))
                .andExpect(jsonPath("$.order.products").isNotEmpty());
    }

    @Test
    void find_order_by_uuid_not_found_fail() throws Exception {
        String url = API_ORDER_CANCEL + ORDER_NOT_EXISTS;
        doGet(url, null)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value("order.notFound.error"))
                .andExpect(jsonPath("$.message").value("Order with uuid=" + ORDER_NOT_EXISTS + " not found!"));
    }

    @Test
    void delete_order_by_uuid_success() throws Exception {
        String url = API_ORDER + ORDER_UUID_4;
        doDelete(url, null)
                .andExpect(status().isOk());
    }

    @Test
    void delete_order_by_uuid_not_found_fail() throws Exception {
        String url = API_ORDER + ORDER_NOT_EXISTS;
        doDelete(url, null)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value("order.notFound.error"))
                .andExpect(jsonPath("$.message").value("Order with uuid=" + ORDER_NOT_EXISTS + " not found!"));
    }

    private Set<ProductRequestDto> generateProductRequests() {
        return Stream
                .of(
                        new ProductRequestDto()
                                .setUuid(PRODUCT_UUID_1)
                                .setCount(1))
                .collect(Collectors.toSet());
    }

    private Set<ProductRequestDto> generateProductRequests(int count) {
        return Stream
                .of(
                        new ProductRequestDto()
                                .setUuid(PRODUCT_UUID_1)
                                .setCount(count))
                .collect(Collectors.toSet());
    }
}
