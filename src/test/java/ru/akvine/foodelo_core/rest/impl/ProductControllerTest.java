package ru.akvine.foodelo_core.rest.impl;

import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;
import ru.akvine.foodelo_core.ApiBaseTest;
import ru.akvine.foodelo_core.rest.dto.request.product.AddProductRequest;
import ru.akvine.foodelo_core.rest.dto.request.product.GetProductsRequest;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.akvine.foodelo_core.RestMethods.API_PRODUCTS_ADD;
import static ru.akvine.foodelo_core.RestMethods.API_PRODUCTS_LIST;
import static ru.akvine.foodelo_core.TestConstants.*;

public class ProductControllerTest extends ApiBaseTest {

    @Test
    void get_list_success() throws Exception {
        GetProductsRequest request = new GetProductsRequest()
                .setPage(0)
                .setLimits(5);
        doGet(API_PRODUCTS_LIST, request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"))
                .andExpect(jsonPath("$.products").isNotEmpty());
    }

    @Test
    void get_list_fail_page_less_than_zero() throws Exception {
        GetProductsRequest request = new GetProductsRequest()
                .setPage(INVALID_PAGE)
                .setLimits(5);
        doGet(API_PRODUCTS_LIST, request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value("validation.error"))
                .andExpect(jsonPath("$.message").value("Request's page less than 0"));
    }

    @Test
    void get_list_fail_limits_less_than_zero() throws Exception {
        GetProductsRequest request = new GetProductsRequest()
                .setPage(PRODUCT_PAGE)
                .setLimits(INVALID_LIMITS);
        doGet(API_PRODUCTS_LIST, request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value("validation.error"))
                .andExpect(jsonPath("$.message").value("Request's limits less then 0"));
    }

    @Test
    @Transactional
    void add_product_success() throws Exception {
        Date EXPIRATION_DATE_1 = new SimpleDateFormat("yyyy-MM-dd").parse("2022-25-05");
        Date PRODUCTION_DATE_1 = new SimpleDateFormat("yyyy-MM-dd").parse("2022-08-05");

        AddProductRequest request = new AddProductRequest()
                .setName(PRODUCT_NAME_1)
                .setCategory(PRODUCT_CATEGORY_1)
                .setCount(PRODUCT_COUNT_1)
                .setProducer(PRODUCT_PRODUCER_1)
                .setPrice(4500)
                .setExpirationDate(EXPIRATION_DATE_1)
                .setProductionDate(PRODUCTION_DATE_1);
        doPost(API_PRODUCTS_ADD, request)
                .andExpect(status().isOk());
    }

    @Test
    void add_product_fail_count_less_than_zero() throws Exception {
        AddProductRequest request = new AddProductRequest()
                .setName(PRODUCT_NAME_1)
                .setCategory(PRODUCT_CATEGORY_1)
                .setCount(INVALID_PRODUCT_COUNT)
                .setProducer(PRODUCT_PRODUCER_1)
                .setPrice(PRODUCT_PRICE_1)
                .setExpirationDate(new Date())
                .setProductionDate(new Date());
        doPost(API_PRODUCTS_ADD, request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value("validation.error"))
                .andExpect(jsonPath("$.message").value("Product count can't be less than 0"));
    }

    @Test
    void add_product_fail_price_less_than_zero() throws Exception {
        Date EXPIRATION_DATE_1 = new SimpleDateFormat("yyyy-MM-dd").parse("2022-25-05");
        Date PRODUCTION_DATE_1 = new SimpleDateFormat("yyyy-MM-dd").parse("2022-08-05");

        AddProductRequest request = new AddProductRequest()
                .setName(PRODUCT_NAME_1)
                .setCategory(PRODUCT_CATEGORY_1)
                .setCount(PRODUCT_COUNT_1)
                .setProducer(PRODUCT_PRODUCER_1)
                .setPrice(INVALID_PRODUCT_PRICE)
                .setExpirationDate(EXPIRATION_DATE_1)
                .setProductionDate(PRODUCTION_DATE_1);
        doPost(API_PRODUCTS_ADD, request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value("validation.error"))
                .andExpect(jsonPath("$.message").value("Product price can't be less than 0"));
    }

    @Test
    void add_product_fail_production_date_after_expiration_date() throws Exception {
        Date EXPIRATION_DATE_1 = new SimpleDateFormat("yyyy-MM-dd").parse("2022-25-05");
        Date PRODUCTION_DATE_1 = new SimpleDateFormat("yyyy-MM-dd").parse("2022-08-05");

        AddProductRequest request = new AddProductRequest()
                .setName(PRODUCT_NAME_1)
                .setCategory(PRODUCT_CATEGORY_1)
                .setCount(PRODUCT_COUNT_1)
                .setProducer(PRODUCT_PRODUCER_1)
                .setPrice(PRODUCT_PRICE_1)
                .setExpirationDate(PRODUCTION_DATE_1)
                .setProductionDate(EXPIRATION_DATE_1);
        doPost(API_PRODUCTS_ADD, request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value("validation.error"))
                .andExpect(jsonPath("$.message").value("Production date can't be later than the expiration date"));
    }

    @Test
    void get_by_uuid_success() throws Exception {
        String url = API_PRODUCTS_LIST + "/" + PRODUCT_UUID_1;
        doGet(url, null)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"))
                .andExpect(jsonPath("$.product.count").value(100))
                .andExpect(jsonPath("$.product.price").value(8400))
                .andExpect(jsonPath("$.product.deleted").value(false));

    }
}