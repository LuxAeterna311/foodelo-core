package ru.akvine.foodelo_core.repository;

import org.junit.jupiter.api.Test;
import ru.akvine.foodelo_core.DataBaseTest;
import ru.akvine.foodelo_core.domain.entity.OrderEntity;
import ru.akvine.foodelo_core.domain.entity.ProductItemEntity;
import ru.akvine.foodelo_core.domain.models.OrderStatus;
import ru.akvine.foodelo_core.util.UUIDGeneratorUtil;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class OrderRepositoryTest extends DataBaseTest {

    @Test
    void should_save_order() {
        String uuid = UUIDGeneratorUtil.generate("-" );
        List<ProductItemEntity> products = Stream
                .of(
                new ProductItemEntity(),
                new ProductItemEntity())
                .collect(Collectors.toList());
        OrderEntity orderEntity = new OrderEntity()
                .setUuid(uuid)
                .setStatus(OrderStatus.CREATED)
                .setAmount(3200)
                .setUpdatedDate(new Date())
                .setDeletedDate(null)
                .setDeleted(false)
                .setProducts(products);

        OrderEntity savedOrderEntity = orderRepository.save(orderEntity);
        assertEquals(orderEntity, savedOrderEntity);
        assertEquals(uuid, savedOrderEntity.getUuid());
        assertEquals(orderEntity.getAmount(), savedOrderEntity.getAmount());
        assertEquals(orderEntity.getStatus(), OrderStatus.CREATED);
        assertEquals(orderEntity.getUpdatedDate(), savedOrderEntity.getUpdatedDate());
    }

    @Test
    void should_find_order_by_uuid() {
        String uuid = UUIDGeneratorUtil.generate("-" );
        List<ProductItemEntity> products = Stream
                .of(
                        new ProductItemEntity(),
                        new ProductItemEntity())
                .collect(Collectors.toList());
        OrderEntity orderEntity = new OrderEntity()
                .setUuid(uuid)
                .setStatus(OrderStatus.CREATED)
                .setAmount(3200)
                .setUpdatedDate(new Date())
                .setDeletedDate(null)
                .setDeleted(false)
                .setProducts(products);
        orderRepository.save(orderEntity);

        OrderEntity foundedEntity = orderRepository.findByUuid(uuid).get();
        assertEquals(orderEntity, foundedEntity);
        assertNotEquals(null, orderEntity.getId());
        assertEquals(orderEntity.getAmount(), foundedEntity.getAmount());
    }

    @Test
    void should_not_find_deleted_order() {
        String uuid = UUIDGeneratorUtil.generate("-" );
        List<ProductItemEntity> products = Stream
                .of(
                        new ProductItemEntity(),
                        new ProductItemEntity())
                .collect(Collectors.toList());
        OrderEntity orderEntity = new OrderEntity()
                .setUuid(uuid)
                .setStatus(OrderStatus.CREATED)
                .setAmount(3200)
                .setUpdatedDate(new Date())
                .setDeletedDate(new Date())
                .setDeleted(true)
                .setProducts(products);
        orderRepository.save(orderEntity);

        NoSuchElementException exception = assertThrows(NoSuchElementException.class, () -> orderRepository
                .findByUuid(uuid)
                .get());

        assertEquals("No value present", exception.getMessage());
    }
}
