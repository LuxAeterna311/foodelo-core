package ru.akvine.foodelo_core.repository;

import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import ru.akvine.foodelo_core.DataBaseTest;
import ru.akvine.foodelo_core.domain.entity.ProductEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.akvine.foodelo_core.TestConstants.*;
import static ru.akvine.foodelo_core.util.UUIDGeneratorUtil.generate;

class ProductRepositoryTest extends DataBaseTest {

    @Test
    void should_save_product() {
        ProductEntity productEntity = generateTestProduct();
        ProductEntity savedProductEntity = productRepository.save(productEntity);
        assertEquals(productEntity, savedProductEntity);
    }

    @Test
    void should_find_by_uuid() {
        String generatedUuid = generate("-");
        ProductEntity productEntity = new ProductEntity()
                .setUuid(generatedUuid)
                .setName(PRODUCT_NAME_1)
                .setProducer(PRODUCT_PRODUCER_1)
                .setCategory(PRODUCT_CATEGORY_1)
                .setPrice(PRODUCT_PRICE_1)
                .setCount(PRODUCT_COUNT_1)
                .setProductionDate(PRODUCTION_DATE_1)
                .setExpirationDate(EXPIRATION_DATE_1)
                .setUpdatedDate(new Date())
                .setDeleted(false);
        productRepository.save(productEntity);
        ProductEntity foundedProductEntity = productRepository
                .findByUuid(generatedUuid)
                .get();

        assertEquals(productEntity, foundedProductEntity);
        assertEquals(foundedProductEntity.getCategory(), PRODUCT_CATEGORY_1);
        assertEquals(foundedProductEntity.getName(), PRODUCT_NAME_1);
        assertEquals(foundedProductEntity.getPrice(), PRODUCT_PRICE_1);
        assertEquals(foundedProductEntity.getProducer(), PRODUCT_PRODUCER_1);
        assertEquals(foundedProductEntity.getCount(), PRODUCT_COUNT_1);
    }

    @Test
    void should_find_all_not_deleted() {
        int count = 5;
        Pageable pageable = PageRequest.of(0, count);
        List<ProductEntity> products = generateTestProduct(count);
        products.get(0).setDeleted(true);
        productRepository.saveAll(products);
        List<ProductEntity> findedProducts = productRepository.findAllAndNotDeleted(pageable);

        assertEquals(count - 1, findedProducts.size());
    }

    @Test
    void should_find_by_category() {
        int count = 5;
        List<ProductEntity> products = generateTestProduct(count);
        productRepository.saveAll(products);
        List<ProductEntity> findedProducts = productRepository.findByCategory(PRODUCT_CATEGORY_1);
        assertEquals(count, findedProducts.size());
    }

    @Test
    void should_delete_from_db() {
        ProductEntity productEntity = generateTestProduct();
        String generatedUuid = productEntity.getUuid();
        Pageable pageable = PageRequest.of(0, 5);
        productRepository.save(productEntity);
        List<ProductEntity> products = productRepository.findAllAndNotDeleted(pageable);
        assertEquals(1, products.size());

        productRepository.deleteByUuid(generatedUuid, new Date());
        List<ProductEntity> productsAfterDeleting = productRepository.findAllAndNotDeleted(pageable);
        assertEquals(0, productsAfterDeleting.size());
    }

    public List<ProductEntity> generateTestProduct(int count) {
        List<ProductEntity> products = new ArrayList<>();
        for (int i = 0; i < count; ++i) {
            ProductEntity productEntity = new ProductEntity()
                    .setUuid(generate("-"))
                    .setName(PRODUCT_NAME_1)
                    .setProducer(PRODUCT_PRODUCER_1)
                    .setCategory(PRODUCT_CATEGORY_1)
                    .setPrice(PRODUCT_PRICE_1)
                    .setCount(PRODUCT_COUNT_1)
                    .setProductionDate(PRODUCTION_DATE_1)
                    .setExpirationDate(EXPIRATION_DATE_1)
                    .setUpdatedDate(new Date())
                    .setDeleted(false);

            products.add(productEntity);
        }

        return products;
    }

    public ProductEntity generateTestProduct() {
        return new ProductEntity()
                .setUuid(generate("-"))
                .setName(PRODUCT_NAME_1)
                .setProducer(PRODUCT_PRODUCER_1)
                .setCategory(PRODUCT_CATEGORY_1)
                .setPrice(PRODUCT_PRICE_1)
                .setCount(PRODUCT_COUNT_1)
                .setProductionDate(PRODUCTION_DATE_1)
                .setExpirationDate(EXPIRATION_DATE_1)
                .setUpdatedDate(new Date())
                .setDeleted(false);
    }
}
