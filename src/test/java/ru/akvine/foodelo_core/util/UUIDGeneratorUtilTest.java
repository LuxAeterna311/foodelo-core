package ru.akvine.foodelo_core.util;

import org.junit.jupiter.api.Test;
import ru.akvine.foodelo_core.exception.ValidationException;

import static org.junit.jupiter.api.Assertions.*;

class UUIDGeneratorUtilTest {

    @Test
    void generate_without_dashes_fail_length_less_than_0() {
        int length = -15;
        Exception exception = assertThrows(ValidationException.class, () -> UUIDGeneratorUtil.generate(length, "-"));
        String expectedMessage = "Length can't be less -1 or more than generated uuid";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void generate_without_dashes_fail_length_more_than_generated_uuid() {
        int length = 1500;
        Exception exception = assertThrows(ValidationException.class, () -> UUIDGeneratorUtil.generate(length, ""));
        String expectedMessage = "Length can't be less -1 or more than generated uuid";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void generate_success() {
        String generatedUuid = UUIDGeneratorUtil.generate();
        assertTrue(generatedUuid.contains("-"));
    }

    @Test
    void generate_without_dashes_success() {
        int length = 15;
        String generatedUuid = UUIDGeneratorUtil.generate(length, "-");
        assertFalse(generatedUuid.contains("-"));
    }
}