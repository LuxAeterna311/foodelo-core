package ru.akvine.foodelo_core.exception.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.akvine.foodelo_core.exception.InvalidReduceException;
import ru.akvine.foodelo_core.exception.OrderNotFoundException;
import ru.akvine.foodelo_core.exception.ProductNotFoundException;
import ru.akvine.foodelo_core.exception.ValidationException;
import ru.akvine.foodelo_core.response.CommonErrorCodes;
import ru.akvine.foodelo_core.response.ErrorResponse;

import java.util.Date;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler({ProductNotFoundException.class})
    public ResponseEntity<ErrorResponse> handleProductNotFoundException(ProductNotFoundException exception) {
        logger.error("Product not found error", exception);
        ErrorResponse errorResponse = new ErrorResponse(CommonErrorCodes.PRODUCT_NOT_FOUND,
                exception.getMessage(),
                new Date());
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ValidationException.class})
    public ResponseEntity<ErrorResponse> handleValidationException(ValidationException exception) {
        logger.error("Validation error", exception);
        ErrorResponse errorResponse = new ErrorResponse(CommonErrorCodes.VALIDATION_ERROR,
                exception.getMessage(),
                new Date());
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({OrderNotFoundException.class})
    public ResponseEntity<ErrorResponse> handleOrderNotFoundException(OrderNotFoundException exception) {
        logger.error("Order not found error", exception);
        ErrorResponse errorResponse = new ErrorResponse(CommonErrorCodes.ORDER_NOT_FOUND,
                exception.getMessage(),
                new Date());
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({InvalidReduceException.class})
    public ResponseEntity<ErrorResponse> handleInvalidReduceException(InvalidReduceException exception) {
        logger.error("Can't reduce product's count", exception);
        ErrorResponse errorResponse = new ErrorResponse(CommonErrorCodes.REDUCE_COUNT_ERROR,
                exception.getMessage(),
                new Date());
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
