package ru.akvine.foodelo_core.exception;

public class InvalidReduceException extends RuntimeException {
    public InvalidReduceException(String message) {
        super(message);
    }
}
