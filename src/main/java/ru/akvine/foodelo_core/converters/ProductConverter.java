package ru.akvine.foodelo_core.converters;

import com.google.common.base.Preconditions;
import org.springframework.stereotype.Component;
import ru.akvine.foodelo_core.rest.dto.converter.AddProduct;
import ru.akvine.foodelo_core.rest.dto.converter.GetProducts;
import ru.akvine.foodelo_core.rest.dto.converter.ProductDto;
import ru.akvine.foodelo_core.rest.dto.request.product.AddProductRequest;
import ru.akvine.foodelo_core.rest.dto.request.product.GetProductsRequest;
import ru.akvine.foodelo_core.rest.dto.response.ProductListResponse;
import ru.akvine.foodelo_core.rest.dto.response.ProductResponse;
import ru.akvine.foodelo_core.service.dto.Product;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductConverter {
    public ProductResponse convertToProductResponse(Product product) {
        ProductDto productDto = new ProductDto(product);
        return new ProductResponse()
                .setProduct(productDto);
    }

    public AddProduct convertToAddProduct(AddProductRequest request) {
        Preconditions.checkNotNull(request, "request is null");
        Preconditions.checkNotNull(request.getName(), "request.name is null");
        Preconditions.checkNotNull(request.getCategory(), "request.category is null");
        Preconditions.checkNotNull(request.getProductionDate(), "request.productionDate is null");
        Preconditions.checkNotNull(request.getExpirationDate(), "request.expirationDate is null");
        Preconditions.checkNotNull(request.getProducer(), "request.count is null");

        return new AddProduct()
                .setName(request.getName())
                .setCategory(request.getCategory())
                .setCount(request.getCount())
                .setDescription(request.getDescription())
                .setExpirationDate(request.getExpirationDate())
                .setProductionDate(request.getProductionDate())
                .setPrice(request.getPrice())
                .setProducer(request.getProducer());
    }

    public ProductListResponse convertToProductListResponse(List<Product> products) {
        List<ProductDto> productsDto = products
                .stream()
                .map(ProductDto::new)
                .collect(Collectors.toList());
        return new ProductListResponse()
                .setProducts(productsDto);
    }

    public GetProducts convertToGetProducts(GetProductsRequest request) {
        Preconditions.checkNotNull(request, "request is null");
        Preconditions.checkNotNull(request.getPage(), "request.page is null");
        Preconditions.checkNotNull(request.getLimits(), "request.limit is null");

        return new GetProducts()
                .setPage(request.getPage())
                .setLimits(request.getLimits());
    }
}
