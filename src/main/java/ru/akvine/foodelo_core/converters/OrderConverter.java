package ru.akvine.foodelo_core.converters;

import com.google.common.base.Preconditions;
import org.springframework.stereotype.Component;
import ru.akvine.foodelo_core.rest.dto.converter.OrderDto;
import ru.akvine.foodelo_core.rest.dto.converter.PerformOrder;
import ru.akvine.foodelo_core.rest.dto.request.order.PerformOrderRequest;
import ru.akvine.foodelo_core.rest.dto.response.OrderResponse;
import ru.akvine.foodelo_core.service.dto.Order;

import java.util.HashMap;
import java.util.Map;

@Component
public class OrderConverter {
    public PerformOrder convertToPerformOrder(PerformOrderRequest request) {
        Preconditions.checkNotNull(request, "request is null");
        Preconditions.checkNotNull(request.getProducts(), "request.products is null");

        Map<String, Integer> products = new HashMap<>();
        request.getProducts().forEach(obj -> products.put(obj.getUuid(), obj.getCount()));

        return new PerformOrder()
                .setProducts(products);
    }

    public OrderResponse convertToOrderResponse(Order order) {
        Preconditions.checkNotNull(order, "order is null");

        OrderDto orderDto = new OrderDto()
                .setId(order.getId())
                .setUuid(order.getUuid())
                .setStatus(order.getStatus())
                .setAmount(order.getAmount())
                .setProducts(order.getProducts());

        return new OrderResponse()
                .setOrder(orderDto);
    }

}
