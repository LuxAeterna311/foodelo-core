package ru.akvine.foodelo_core.validator;

public interface Validator<T> {
    void validate(T obj);
}
