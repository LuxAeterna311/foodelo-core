package ru.akvine.foodelo_core.rest.meta;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.akvine.foodelo_core.response.Response;
import ru.akvine.foodelo_core.rest.dto.request.LoginRequest;
import ru.akvine.foodelo_core.rest.dto.request.RegisterRequest;

import javax.validation.Valid;

@RequestMapping("api/v1/clients")
public interface ClientControllerMeta {
    @GetMapping("/register")
    Response register(@Valid @RequestBody RegisterRequest request);

    @GetMapping("/login")
    Response login(@Valid @RequestBody LoginRequest request);

    @GetMapping("/logout")
    Response logout();
}
