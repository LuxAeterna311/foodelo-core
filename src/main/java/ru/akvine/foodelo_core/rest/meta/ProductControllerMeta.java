package ru.akvine.foodelo_core.rest.meta;

import org.springframework.web.bind.annotation.*;
import ru.akvine.foodelo_core.response.Response;
import ru.akvine.foodelo_core.rest.dto.request.product.AddProductRequest;
import ru.akvine.foodelo_core.rest.dto.request.product.GetProductsRequest;

import javax.validation.Valid;

@RequestMapping("api/v1/products")
public interface ProductControllerMeta {

    @GetMapping
    Response getList(@Valid @RequestBody GetProductsRequest request);

    @GetMapping("/{uuid}")
    Response getByUuid(@PathVariable("uuid") String uuid);

    @GetMapping("/category/{uuid}")
    Response getListByCategory(@PathVariable("uuid") String uuid);

    @PostMapping
    String add(@Valid @RequestBody AddProductRequest request);

    @DeleteMapping("/{uuid}")
    Response delete(@PathVariable("uuid") String uuid);
}
