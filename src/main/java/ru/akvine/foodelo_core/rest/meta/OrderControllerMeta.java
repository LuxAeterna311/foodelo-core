package ru.akvine.foodelo_core.rest.meta;

import org.springframework.web.bind.annotation.*;
import ru.akvine.foodelo_core.response.Response;
import ru.akvine.foodelo_core.rest.dto.request.order.PaymentOrderRequest;
import ru.akvine.foodelo_core.rest.dto.request.order.PerformOrderRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RequestMapping("api/v1/orders")
public interface OrderControllerMeta {

    @PostMapping("/perform")
    Response performOrder(@Valid @RequestBody PerformOrderRequest request);

    @GetMapping("/payment")
    Response paymentOrder(@Valid @RequestBody PaymentOrderRequest request);

    @GetMapping("/cancel/{uuid}")
    Response cancelOrder(@NotBlank @PathVariable("uuid") String uuid);

    @GetMapping("/{uuid}")
    Response findByUuid(@NotBlank @PathVariable("uuid") String uuid);

    @DeleteMapping("/{uuid}")
    void deleteByUuid(@NotBlank @PathVariable("uuid") String uuid);
}
