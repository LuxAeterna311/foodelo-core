package ru.akvine.foodelo_core.rest.dto.response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import ru.akvine.foodelo_core.response.SuccessfulResponse;
import ru.akvine.foodelo_core.rest.dto.converter.ProductDto;

import java.util.List;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class ProductListResponse extends SuccessfulResponse {
    private List<ProductDto> products;
}
