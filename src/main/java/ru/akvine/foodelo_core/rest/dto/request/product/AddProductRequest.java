package ru.akvine.foodelo_core.rest.dto.request.product;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class AddProductRequest {

    private String name;

    private String category;

    private String producer;

    private String description;

    private Date productionDate;

    private Date expirationDate;

    private int count;

    private long price;
}
