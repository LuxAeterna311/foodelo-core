package ru.akvine.foodelo_core.rest.dto.converter;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.akvine.foodelo_core.service.dto.ProductItem;

import java.util.List;

@Data
@Accessors(chain = true)
public class OrderDto {
    private Long id;
    private String uuid;
    private String status;
    private List<ProductItem> products;
    private long amount;
}
