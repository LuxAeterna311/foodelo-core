package ru.akvine.foodelo_core.rest.dto.response;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.akvine.foodelo_core.response.SuccessfulResponse;
import ru.akvine.foodelo_core.rest.dto.converter.ProductDto;

@Data
@Accessors(chain = true)
public class ProductResponse extends SuccessfulResponse {
    private ProductDto product;
}
