package ru.akvine.foodelo_core.rest.dto.converter;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import ru.akvine.foodelo_core.service.dto.Product;

import java.math.BigInteger;
import java.util.Date;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class ProductDto {
    private String uuid;
    private String name;
    private String category;
    private String producer;
    private String description;
    private Date productionDate;
    private Date expirationDate;
    private int count;
    private long price;
    private Date updatedDate;
    private Date deletedDate;
    private boolean deleted;

    public ProductDto(Product product) {
        this.uuid = product.getUuid();
        this.name = product.getName();
        this.category = product.getCategory();
        this.description = product.getDescription();
        this.productionDate = product.getProductionDate();
        this.expirationDate = product.getExpirationDate();
        this.producer = product.getProducer();
        this.count = product.getCount();
        this.price = product.getPrice();
        this.updatedDate = product.getUpdatedDate();
        this.deletedDate = product.getDeletedDate();
        this.deleted = product.isDeleted();
    }
}
