package ru.akvine.foodelo_core.rest.dto.request.product;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class GetProductsRequest {
    private Integer page;
    private Integer limits;
}
