package ru.akvine.foodelo_core.rest.dto.request.product;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ProductRequestDto {
    private String uuid;
    private int count;
}
