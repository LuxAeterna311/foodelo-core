package ru.akvine.foodelo_core.rest.dto.request.order;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.akvine.foodelo_core.rest.dto.request.product.ProductRequestDto;

import java.util.Set;

@Data
@Accessors(chain = true)
public class PerformOrderRequest {
    private Set<ProductRequestDto> products;
}
