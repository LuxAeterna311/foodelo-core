package ru.akvine.foodelo_core.rest.dto.response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import ru.akvine.foodelo_core.response.SuccessfulResponse;
import ru.akvine.foodelo_core.rest.dto.converter.OrderDto;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class OrderResponse extends SuccessfulResponse {
    private OrderDto order;
}
