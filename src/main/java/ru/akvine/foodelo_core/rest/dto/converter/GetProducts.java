package ru.akvine.foodelo_core.rest.dto.converter;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class GetProducts {
    private Integer page;
    private Integer limits;
}
