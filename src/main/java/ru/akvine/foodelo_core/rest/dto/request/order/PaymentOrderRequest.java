package ru.akvine.foodelo_core.rest.dto.request.order;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PaymentOrderRequest {
}
