package ru.akvine.foodelo_core.rest.dto.converter;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;

@Data
@Accessors(chain = true)
public class PerformOrder {
    private Map<String, Integer> products;
}
