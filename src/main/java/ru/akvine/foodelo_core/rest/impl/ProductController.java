package ru.akvine.foodelo_core.rest.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.akvine.foodelo_core.converters.ProductConverter;
import ru.akvine.foodelo_core.response.Response;
import ru.akvine.foodelo_core.response.SuccessfulResponse;
import ru.akvine.foodelo_core.rest.dto.converter.AddProduct;
import ru.akvine.foodelo_core.rest.dto.converter.GetProducts;
import ru.akvine.foodelo_core.rest.dto.request.product.AddProductRequest;
import ru.akvine.foodelo_core.rest.dto.request.product.GetProductsRequest;
import ru.akvine.foodelo_core.rest.meta.ProductControllerMeta;
import ru.akvine.foodelo_core.service.ProductService;
import ru.akvine.foodelo_core.service.dto.Product;
import ru.akvine.foodelo_core.rest.validator.ProductValidator;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class ProductController implements ProductControllerMeta {
    private final ProductService productService;
    private final ProductConverter productConverter;
    private final ProductValidator productValidator;

    @Override
    public Response getList(@Valid @RequestBody GetProductsRequest request) {
        productValidator.verifyGetProductsRequest(request);
        GetProducts getProducts = productConverter.convertToGetProducts(request);
        List<Product> products = productService.getList(getProducts);
        return productConverter.convertToProductListResponse(products);
    }

    @Override
    public Response getByUuid(@NotBlank String uuid) {
        Product product = productService.getByUuid(uuid);
        return productConverter.convertToProductResponse(product);
    }

    @Override
    public Response getListByCategory(@NotBlank String category) {
        List<Product> products = productService.getListByCategory(category);
        return productConverter.convertToProductListResponse(products);
    }

    @Override
    public String add(@Valid @RequestBody AddProductRequest request) {
        productValidator.verifyAddProductRequest(request);
        AddProduct addProduct = productConverter.convertToAddProduct(request);
        return productService.save(addProduct);
    }

    @Override
    public Response delete(@NotBlank String uuid) {
        productService.deleteByUuid(uuid);
        return new SuccessfulResponse();
    }
}
