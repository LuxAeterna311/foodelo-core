package ru.akvine.foodelo_core.rest.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.akvine.foodelo_core.converters.OrderConverter;
import ru.akvine.foodelo_core.response.Response;
import ru.akvine.foodelo_core.rest.dto.converter.PerformOrder;
import ru.akvine.foodelo_core.rest.dto.request.order.PaymentOrderRequest;
import ru.akvine.foodelo_core.rest.dto.request.order.PerformOrderRequest;
import ru.akvine.foodelo_core.rest.meta.OrderControllerMeta;
import ru.akvine.foodelo_core.rest.validator.OrderValidator;
import ru.akvine.foodelo_core.service.OrderService;
import ru.akvine.foodelo_core.service.dto.Order;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
@RequiredArgsConstructor
public class OrderController implements OrderControllerMeta {
    private final OrderValidator orderValidator;
    private final OrderConverter orderConverter;
    private final OrderService orderService;

    @Override
    public Response performOrder(@Valid @RequestBody PerformOrderRequest request) {
        orderValidator.verifyPerformOrderRequest(request);
        PerformOrder performOrder = orderConverter.convertToPerformOrder(request);
        Order order = orderService.performOrder(performOrder);
        return orderConverter.convertToOrderResponse(order);
    }

    @Override
    public Response paymentOrder(@Valid @RequestBody PaymentOrderRequest request) {
        return null;
    }

    @Override
    public Response cancelOrder(@NotBlank String uuid) {
        Order order = orderService.cancelOrder(uuid);
        return orderConverter.convertToOrderResponse(order);
    }

    @Override
    public Response findByUuid(@NotBlank String uuid) {
        Order order = orderService.getByUuid(uuid);
        return orderConverter.convertToOrderResponse(order);
    }

    @Override
    public void deleteByUuid(@NotBlank String uuid) {
        orderService.deleteByUuid(uuid);
    }
}
