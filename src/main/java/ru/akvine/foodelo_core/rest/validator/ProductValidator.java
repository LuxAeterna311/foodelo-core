package ru.akvine.foodelo_core.rest.validator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.akvine.foodelo_core.exception.ValidationException;
import ru.akvine.foodelo_core.rest.dto.request.product.AddProductRequest;
import ru.akvine.foodelo_core.rest.dto.request.product.GetProductsRequest;

@Component
@Slf4j
public class ProductValidator {
    public void verifyGetProductsRequest(GetProductsRequest request) {
        if (request == null || request.getPage() == null || request.getLimits() == null) {
            logger.info("Request or request's parameters are null");
            throw new ValidationException("Request or request's parameters are null");
        }
        if (request.getLimits() < 0) {
            logger.info("Request's limits less then 0");
            throw new ValidationException("Request's limits less then 0");
        }
        if (request.getPage() < 0) {
            logger.info("Request's page less than 0");
            throw new ValidationException("Request's page less than 0");
        }
    }

    public void verifyAddProductRequest(AddProductRequest request) {
        if (request.getPrice() < 0) {
            logger.info("Product price can't be less than 0");
            throw new ValidationException("Product price can't be less than 0");
        }
        if (request.getProductionDate().after(request.getExpirationDate())) {
            logger.info("Production date can't be later than the expiration date");
            throw new ValidationException("Production date can't be later than the expiration date");
        }
        if (request.getCount() < 0) {
            logger.info("Product count can't be less than 0");
            throw new ValidationException("Product count can't be less than 0");
        }
    }
}
