package ru.akvine.foodelo_core.rest.validator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.akvine.foodelo_core.exception.ValidationException;
import ru.akvine.foodelo_core.rest.dto.request.order.PerformOrderRequest;
import ru.akvine.foodelo_core.rest.dto.request.product.ProductRequestDto;

import java.util.Set;

@Component
@Slf4j
public class OrderValidator {
    public void verifyPerformOrderRequest(PerformOrderRequest request) {
        Set<ProductRequestDto> products = request.getProducts();
        for (ProductRequestDto product : products) {
            if (product.getCount() <= 0) {
                logger.info("Validation error occurred");
                throw new ValidationException("Product's count can't be less than 0");
            }
        }
    }
}
