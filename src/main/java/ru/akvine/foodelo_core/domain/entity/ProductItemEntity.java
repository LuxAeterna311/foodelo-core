package ru.akvine.foodelo_core.domain.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Table(name = "PRODUCT_ITEM")
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class ProductItemEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "productItemSeq")
    @SequenceGenerator(name = "productItemSeq", sequenceName = "SEQ_PRODUCT_ITEM", allocationSize = 1000)
    @Column(name = "ID", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private OrderEntity order;

    @OneToOne
    @JoinColumn(name = "PRODUCT_ID")
    private ProductEntity product;

    @Column(name = "COUNT", nullable = false)
    private int count;
}
