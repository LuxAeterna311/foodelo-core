package ru.akvine.foodelo_core.domain.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "PRODUCT")
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class ProductEntity {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "productSeq")
    @SequenceGenerator(name = "productSeq", sequenceName = "SEQ_PRODUCT", allocationSize = 1000)
    private Long id;

    @Column(name = "UUID", nullable = false)
    private String uuid;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "CATEGORY", nullable = false)
    private String category;

    @Column(name = "MEASURE", nullable = false)
    private long measure;

    @Column(name = "PRODUCER", nullable = false)
    private String producer;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "PRODUCTION_DATE")
    private Date productionDate;

    @Column(name = "EXPIRATION_DATE")
    private Date expirationDate;

    @Column(name = "COUNT", nullable = false)
    private int count;

    @Column(name = "PRICE", nullable = false)
    private long price;

    @Column(name = "UPDATED_DATE", nullable = false)
    private Date updatedDate;

    @Column(name = "DELETED_DATE")
    private Date deletedDate;

    @Column(name = "IS_DELETED", nullable = false)
    private boolean deleted;
}
