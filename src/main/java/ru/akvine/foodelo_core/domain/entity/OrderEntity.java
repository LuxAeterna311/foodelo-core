package ru.akvine.foodelo_core.domain.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import ru.akvine.foodelo_core.domain.models.OrderStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "ORDER_DATA")
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "orderDataSeq")
    @SequenceGenerator(name = "orderDataSeq", sequenceName = "SEQ_ORDER_DATA", allocationSize = 1000)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "UUID", nullable = false)
    private String uuid;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProductItemEntity> products;

    @Column(name = "AMOUNT", nullable = false)
    private long amount;

    @Column(name = "UPDATED_DATE", nullable = false)
    private Date updatedDate;

    @Column(name = "DELETED_DATE")
    private Date deletedDate;

    @Column(name = "IS_DELETED", nullable = false)
    private boolean deleted;
}
