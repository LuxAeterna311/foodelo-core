package ru.akvine.foodelo_core.domain.models;

public enum OrderStatus {
    CREATED,
    PAID,
    DECLINED,
}
