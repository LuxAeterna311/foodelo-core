package ru.akvine.foodelo_core.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.akvine.foodelo_core.domain.entity.ProductEntity;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
    @Query("from ProductEntity product " +
            "where product.uuid = :uuid " +
            "and product.deleted = false")
    Optional<ProductEntity> findByUuid(@Param("uuid") String uuid);

    @Query("from ProductEntity product " +
            "where product.category = :category " +
            "and product.deleted = false")
    List<ProductEntity> findByCategory(@Param("category") String category);

    @Modifying
    @Query("update ProductEntity product " +
            "set product.deleted = true, product.deletedDate = :date " +
            "where product.uuid = :uuid " +
            "and product.deleted = false")
    void deleteByUuid(@Param("uuid") String uuid, Date date);

    @Query("from ProductEntity product " +
            "where product.deleted = false")
    List<ProductEntity> findAllAndNotDeleted(Pageable pageable);

    @Query("from ProductEntity product " +
            "where product.uuid in :uuidList")
    List<ProductEntity> findAllByUuidList(@Param("uuidList") List<String> uuidList);
}
