package ru.akvine.foodelo_core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.akvine.foodelo_core.domain.entity.OrderEntity;

import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
    @Query("from OrderEntity order " +
            "where order.uuid = :uuid " +
            "and order.deleted = false")
    Optional<OrderEntity> findByUuid(@Param("uuid") String uuid);
}
