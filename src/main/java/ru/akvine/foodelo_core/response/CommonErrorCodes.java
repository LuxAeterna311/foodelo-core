package ru.akvine.foodelo_core.response;

public class CommonErrorCodes {
    public final static String PRODUCT_NOT_FOUND = "product.notFound.error";
    public final static String ORDER_NOT_FOUND = "order.notFound.error";
    public final static String VALIDATION_ERROR = "validation.error";
    public final static String REDUCE_COUNT_ERROR = "reduce.count.error";
}
