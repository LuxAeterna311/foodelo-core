package ru.akvine.foodelo_core.response;

public interface Response {
    ResponseStatus getStatus();
}
