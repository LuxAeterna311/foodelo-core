package ru.akvine.foodelo_core.response;

public class SuccessfulResponse implements Response {

    @Override
    public ResponseStatus getStatus() {
        return ResponseStatus.SUCCESS;
    }
}
