package ru.akvine.foodelo_core.service;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.akvine.foodelo_core.domain.entity.OrderEntity;
import ru.akvine.foodelo_core.domain.entity.ProductEntity;
import ru.akvine.foodelo_core.domain.entity.ProductItemEntity;
import ru.akvine.foodelo_core.domain.models.OrderStatus;
import ru.akvine.foodelo_core.exception.OrderNotFoundException;
import ru.akvine.foodelo_core.repository.OrderRepository;
import ru.akvine.foodelo_core.rest.dto.converter.PerformOrder;
import ru.akvine.foodelo_core.service.dto.CalculateAmount;
import ru.akvine.foodelo_core.service.dto.Order;
import ru.akvine.foodelo_core.util.UUIDGeneratorUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {
    private final OrderRepository orderRepository;
    private final CalculateService calculateService;
    private final ProductService productService;

    public Order performOrder(PerformOrder performOrder) {
        Preconditions.checkNotNull(performOrder, "performOrder is null");
        Preconditions.checkNotNull(performOrder.getProducts(), "performOrder.products is null");
        logger.info("Registering order=[{}]", performOrder);

        List<ProductItemEntity> productItemList = new ArrayList<>();
        Map<String, Integer> products = performOrder.getProducts();

        CalculateAmount calculatedAmount = calculateService.calculateAmount(performOrder);
        OrderEntity order = new OrderEntity()
                .setUuid(UUIDGeneratorUtil.generate("-"))
                .setAmount(calculatedAmount.getAmount())
                .setStatus(OrderStatus.CREATED)
                .setUpdatedDate(new Date());

        for (String uuid : products.keySet()) {
            for (ProductEntity product : calculatedAmount.getProducts()) {
                if (product.getUuid().equals(uuid)) {
                    ProductItemEntity productItem = new ProductItemEntity()
                            .setCount(products.get(uuid))
                            .setProduct(product)
                            .setOrder(order);

                    productItemList.add(productItem);
                }
            }
        }

        productService.reduceCount(productItemList);
        order.setProducts(productItemList);

        Order savedOrder = new Order(orderRepository.save(order));
        logger.info("Order has been registered with id=[{}]", savedOrder.getId());

        return savedOrder;
    }

    public Order cancelOrder(String uuid) {
        Preconditions.checkNotNull(uuid, "uuid is null");
        logger.info("Cancel order with uuid=[{}]", uuid);

        OrderEntity orderEntity = orderRepository
                .findByUuid(uuid)
                .orElseThrow(() -> new OrderNotFoundException("Order with uuid=" + uuid + " not found!"));

        productService.increaseCount(orderEntity.getProducts());
        orderEntity.setStatus(OrderStatus.DECLINED);
        return new Order(orderRepository.save(orderEntity));
    }

    public Order getByUuid(String uuid) {
        Preconditions.checkNotNull(uuid, "uuid is null");
        logger.info("Get order by uuid=[{}]", uuid);

        return orderRepository
                .findByUuid(uuid)
                .map(Order::new)
                .orElseThrow(() -> new OrderNotFoundException("Order with uuid=" + uuid + " not found!"));
    }

    public void deleteByUuid(String uuid) {
        Preconditions.checkNotNull(uuid, "uuid is null");
        logger.info("Delete order by uuid=[{}]", uuid);

        OrderEntity orderEntity = orderRepository
                .findByUuid(uuid)
                .orElseThrow(() -> new OrderNotFoundException("Order with uuid=" + uuid + " not found!"));

        if (orderEntity.getStatus().equals(OrderStatus.CREATED)) {
            cancelOrder(uuid);
        }

        orderEntity.setDeleted(true);
        orderEntity.setDeletedDate(new Date());

        orderRepository.save(orderEntity);
    }
}
