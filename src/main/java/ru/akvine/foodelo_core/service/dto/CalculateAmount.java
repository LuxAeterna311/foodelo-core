package ru.akvine.foodelo_core.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.akvine.foodelo_core.domain.entity.ProductEntity;

import java.util.List;

@Data
@Accessors(chain = true)
public class CalculateAmount {
    private List<ProductEntity> products;
    private long amount;
}
