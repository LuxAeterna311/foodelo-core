package ru.akvine.foodelo_core.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.akvine.foodelo_core.domain.entity.OrderEntity;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Accessors(chain = true)
public class Order {
    private Long id;
    private String uuid;
    private String status;
    private List<ProductItem> products;
    private long amount;

    public Order(OrderEntity orderEntity) {
        this.id = orderEntity.getId();
        this.uuid = orderEntity.getUuid();
        this.status = orderEntity.getStatus().name();
        this.amount = orderEntity.getAmount();
        this.products = orderEntity
                .getProducts()
                .stream()
                .map(ProductItem::new)
                .collect(Collectors.toList());
    }
}
