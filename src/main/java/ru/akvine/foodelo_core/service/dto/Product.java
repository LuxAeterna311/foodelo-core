package ru.akvine.foodelo_core.service.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import ru.akvine.foodelo_core.domain.entity.ProductEntity;

import java.util.Date;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class Product {
    private Long id;
    private String uuid;
    private String name;
    private String category;
    private String producer;
    private String description;
    private Date productionDate;
    private Date expirationDate;
    private int count;
    private long price;
    private Date updatedDate;
    private Date deletedDate;
    private boolean deleted;

    public Product(ProductEntity productEntity) {
        this.id = productEntity.getId();
        this.uuid = productEntity.getUuid();
        this.name = productEntity.getName();
        this.category = productEntity.getCategory();
        this.description = productEntity.getDescription();
        this.productionDate = productEntity.getProductionDate();
        this.expirationDate = productEntity.getExpirationDate();
        this.producer = productEntity.getProducer();
        this.count = productEntity.getCount();
        this.price = productEntity.getPrice();
        this.updatedDate = productEntity.getUpdatedDate();
        this.deletedDate = productEntity.getDeletedDate();
        this.deleted = productEntity.isDeleted();
    }
}
