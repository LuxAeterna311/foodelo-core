package ru.akvine.foodelo_core.service.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.akvine.foodelo_core.domain.entity.ProductItemEntity;

@Data
@Accessors(chain = true)
public class ProductItem {
    private String name;
    private Integer count;

    public ProductItem(ProductItemEntity productItemEntity) {
        this.name = productItemEntity.getProduct().getName();
        this.count = productItemEntity.getCount();
    }
}
