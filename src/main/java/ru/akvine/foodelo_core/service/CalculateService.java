package ru.akvine.foodelo_core.service;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.akvine.foodelo_core.domain.entity.ProductEntity;
import ru.akvine.foodelo_core.repository.ProductRepository;
import ru.akvine.foodelo_core.rest.dto.converter.PerformOrder;
import ru.akvine.foodelo_core.service.dto.CalculateAmount;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class CalculateService {
    private final ProductRepository productRepository;

    public CalculateAmount calculateAmount(PerformOrder performOrder) {
        Preconditions.checkNotNull(performOrder, "performOrder is null");
        Preconditions.checkNotNull(performOrder.getProducts(), "performOrder is null");
        logger.debug("Calculate amount for order=[{}]", performOrder);

        long amount = 0;
        List<String> uuidList = new ArrayList<>(performOrder.getProducts().keySet());
        List<ProductEntity> products = productRepository.findAllByUuidList(uuidList);

        for (String uuid : uuidList) {
            for (ProductEntity product : products) {
                if (!product.getUuid().equals(uuid)) {
                    break;
                } else {
                    amount += product.getPrice() * performOrder.getProducts().get(uuid);
                }
            }
        }

        logger.debug("Return calculated amount");
        return new CalculateAmount()
                .setProducts(products)
                .setAmount(amount);
    }
}
