package ru.akvine.foodelo_core.service;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.akvine.foodelo_core.domain.entity.ProductEntity;
import ru.akvine.foodelo_core.domain.entity.ProductItemEntity;
import ru.akvine.foodelo_core.exception.InvalidReduceException;
import ru.akvine.foodelo_core.exception.ProductNotFoundException;
import ru.akvine.foodelo_core.repository.ProductRepository;
import ru.akvine.foodelo_core.rest.dto.converter.AddProduct;
import ru.akvine.foodelo_core.rest.dto.converter.GetProducts;
import ru.akvine.foodelo_core.service.dto.Product;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static ru.akvine.foodelo_core.util.UUIDGeneratorUtil.generate;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductService {
    private final ProductRepository productRepository;

    public Product getByUuid(String uuid) {
        Preconditions.checkNotNull(uuid, "uuid is null");
        logger.info("Get product by uuid=[{}]", uuid);
        return productRepository
                .findByUuid(uuid)
                .map(Product::new)
                .orElseThrow(() -> new ProductNotFoundException("Product not found with uuid=" + uuid));
    }

    public String save(AddProduct addProduct) {
        Preconditions.checkNotNull(addProduct, "addProduct is null");
        logger.info("Save product=[{}]", addProduct);

        ProductEntity productEntity = new ProductEntity()
                .setUuid(generate("-"))
                .setName(addProduct.getName())
                .setCategory(addProduct.getCategory())
                .setCount(addProduct.getCount())
                .setDescription(addProduct.getDescription())
                .setExpirationDate(addProduct.getExpirationDate())
                .setPrice(addProduct.getPrice())
                .setUpdatedDate(new Date())
                .setProductionDate(addProduct.getProductionDate())
                .setProducer(addProduct.getProducer())
                .setDeleted(false);

        return productRepository
                .save(productEntity)
                .getUuid();
    }

    public List<Product> getListByCategory(String category) {
        Preconditions.checkNotNull(category, "category is null");
        logger.info("Get products by category=[{}]", category);

        return productRepository
                .findByCategory(category)
                .stream()
                .map(Product::new)
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteByUuid(String uuid) {
        Preconditions.checkNotNull(uuid, "uuid is null");
        logger.info("Delete product by uuid=[{}]", uuid);
        productRepository.deleteByUuid(uuid, new Date());
    }

    public List<Product> getList(GetProducts getProducts) {
        Pageable pageable = PageRequest.of(getProducts.getPage(), getProducts.getLimits());
        logger.info("Get page limited products");

        return productRepository
                .findAllAndNotDeleted(pageable)
                .stream()
                .map(Product::new)
                .collect(Collectors.toList());
    }

    public void reduceCount(List<ProductItemEntity> productItems) {
        Preconditions.checkNotNull(productItems, "productItems is null");
        List<ProductEntity> products = new ArrayList<>();
        logger.info("Reduce products count, productItems=[{}]", productItems);

        for (ProductItemEntity productItemEntity : productItems) {
            if (productItemEntity.getCount() > productItemEntity.getProduct().getCount()) {
                throw new InvalidReduceException("Reduce count can't be more than product count");
            }

            ProductEntity productEntity = productItemEntity.getProduct();
            int reduceCount = productItemEntity.getCount();
            int productCount = productEntity.getCount();
            int productCountResult = productCount - reduceCount;
            productEntity.setCount(productCountResult);
            products.add(productEntity);
        }

        productRepository.saveAll(products);
    }

    public void increaseCount(List<ProductItemEntity> productItems) {
        Preconditions.checkNotNull(productItems, "productItems is null");
        List<ProductEntity> products = new ArrayList<>();
        logger.info("Increase products count, productItems=[{}]", productItems);

        for (ProductItemEntity productItemEntity : productItems) {
            products.add(productItemEntity.getProduct());
            ProductEntity productEntity = productItemEntity.getProduct();
            int increaseCount = productItemEntity.getCount();
            int productCount = productEntity.getCount();
            int productCountResult = productCount + increaseCount;
            productEntity.setCount(productCountResult);
            products.add(productEntity);
        }

        productRepository.saveAll(products);
    }
}
