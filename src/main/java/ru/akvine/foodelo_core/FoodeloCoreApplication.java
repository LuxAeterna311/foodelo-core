package ru.akvine.foodelo_core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodeloCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoodeloCoreApplication.class, args);
    }

}
